from django.urls import path
from .views import salespeople_list, delete_salesperson, customer_list, delete_customer, sale_list, delete_sale

urlpatterns = [
    path("salespeople/", salespeople_list, name="salesperson_create"),
    path("customers/", customer_list, name="customer_create"),
    path("sales/", sale_list, name="sale_create"),
    path("salespeople/<int:pk>/", delete_salesperson, name="delete_salesperson"),
    path("customers/<int:pk>/", delete_customer, name="delete_customers"),
    path("sales/<int:pk>/", delete_sale, name="delete_sale"),
]
