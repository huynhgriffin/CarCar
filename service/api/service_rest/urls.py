from django.urls import path
from .views import api_technician, api_technicians, api_appointment, api_appointments, api_appointment_cancelled, api_appointment_completed

urlpatterns = [
    path("technicians/", api_technicians, name="api_technicians"),
    path("technicians/<int:id>/", api_technician, name="api_technician"),
    path("appointments/", api_appointments, name="api_appointments"),
    path("appointments/<int:pk>", api_appointment, name="api_appointment"),
    path("appointments/<int:pk>/cancelled/", api_appointment_cancelled, name="api_appointment_cancelled"),
    path("appointments/<int:pk>/completed/", api_appointment_completed, name="api_appointment_completed"),

]
