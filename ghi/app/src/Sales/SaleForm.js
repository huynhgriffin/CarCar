import React, { useEffect, useState } from 'react';

function SaleForm (props) {
    const handleSubmit = async (event) => {
        event.preventDefault();

        const data = {}
        data.salesperson = salesPerson
        data.customer = customer
        data.automobile = automobile
        data.price = price

        const saleUrl = 'http://localhost:8090/api/sales/'
        const fetchConfig = {
            method: 'post',
            body: JSON.stringify(data),
            headers: {
                'Content-Type': 'application/json'
        },
    };

    const response = await fetch(saleUrl, fetchConfig)
    if (response.ok) {
        const newSale = await response.json();
        setSalesperson('');
        setCustomer('');
        setAutomobile('')
        setPrice('');
    }
  }
  const [salespeople, setSalespeople] = useState([])
  const [customers, setCustomers] = useState([])
  const [automobiles, setAutomobiles] = useState([])
  const [salesPerson, setSalesperson] = useState('')
  const [customer, setCustomer] = useState('')
  const [automobile, setAutomobile] = useState('')
  const [price, setPrice] = useState('')

  const handleSalesperson = event => {
    const value = event.target.value;
        setSalesperson(value)
  }
  const handleCustomer= event => {
    const value = event.target.value;
        setCustomer(value)
  }
  const handleAutomobile = event => {
    const value = event.target.value;
        setAutomobile(value)
  }
  const handlePrice = event => {
    const value = event.target.value;
        setPrice(value)
  }

  const fetchSalespeople = async () => {
    const url = "http://localhost:8090/api/salespeople/"

    const response = await fetch(url);

    if (response.ok) {
        const data = await response.json();
        setSalespeople(data.salesperson)
    }
  }

  const fetchCustomers = async () => {
    const url = "http://localhost:8090/api/customers/"

    const response = await fetch(url);

    if (response.ok) {
        const data = await response.json();
        setCustomers(data.customer)
    }
  }

  const fetchAutomobiles = async () => {
    const url = "http://localhost:8100/api/automobiles/"

    const response = await fetch(url);

    if (response.ok) {
        const data = await response.json();
        setAutomobiles(data.autos)
    }
  }

  useEffect (() => {
      fetchSalespeople();
      fetchCustomers();
      fetchAutomobiles();
}, []);

    return (
        <div className="row">
        <div className="offset-3 col-6">
          <div className="shadow p-4 mt-4">
            <h1>Record a new Sale</h1>
            <form onSubmit={handleSubmit} id="create-sale-form">
              <div className="form-floating mb-3">
                <select onChange={handleSalesperson} placeholder="Salesperson" required type="text" id="salesperson" name="salesperson" className="form-control" value={salesPerson}>
                <option value=''>Choose a Salesperson</option>
                    {salespeople.map(salesperson => {
                        return (
                            <option key={salesperson.id} value={salesperson.id}>
                                {salesperson.first_name} {salesperson.last_name}
                            </option>
                        );
                    })}
                </select>
              </div>
              <div className="form-floating mb-3">
                <select onChange={handleCustomer}placeholder="Customer" required type="text" id="customer" name="customer" className="form-control" value={customer}>
                <option value=''>Choose a Customer</option>
                    {customers.map(customer => {
                        return (
                            <option key={customer.id} value={customer.id}>
                                {customer.first_name} { customer.last_name }
                            </option>
                        );
                    })}
                  </select>
              </div>
              <div className="form-floating mb-3">
                <select onChange={handleAutomobile} placeholder="Employee ID" required type="text" id="automobile" name="automobile" className="form-control" value={automobile}>
                <option value=''>Choose a Automobile VIN</option>
                    {automobiles.map(automobile => {
                        return (
                            <option key={automobile.vin} value={automobile.vin}>
                                {automobile.vin}
                            </option>
                        );
                    })}
                  </select>
              </div>
              <div className="form-floating mb-3">
                <input onChange={handlePrice} placeholder="Price" required type="text" id="price" name="price" className="form-control" value={price}/>
                <label htmlFor="price">Price</label>
              </div>
              <button className="btn btn-primary">Create</button>
            </form>
          </div>
        </div>
      </div>
    )

}

export default SaleForm
