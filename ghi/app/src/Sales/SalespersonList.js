import React, { useEffect, useState } from 'react';

function SalespersonList(saleperson) {
    const [salespersonList, setSalespersonList] = useState([])
    async function loadSalesperson() {
        const response = await fetch("http://localhost:8090/api/salespeople/");
        if (response.ok){
            const data = await response.json();
            setSalespersonList(data.salesperson)
        }
        else{
            console.error(response);
        }
    }
    useEffect (() => {
        loadSalesperson()
    }, []);

    return (
        <div>
        <h1>Salespeople</h1>
        <table className="table table-striped">
        <thead>
            <tr>
            <th>Frist Name</th>
            <th>Last Name</th>
            <th>Employee ID</th>
            </tr>
        </thead>
        <tbody>
            {salespersonList.map(salesperson => {
                return (
                <tr key={salesperson.id}>
                <td>{ salesperson.first_name }</td>
                <td>{ salesperson.last_name }</td>
                <td>{ salesperson.employee_id }</td>
            </tr>
            );
        })}
        </tbody>
    </table>
    </div>
    );
}

export default SalespersonList
