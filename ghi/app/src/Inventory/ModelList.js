import React, { useEffect, useState } from 'react';

function ModelList(model) {
    const [modelList, setModelList] = useState ([])
    async function loadModel() {
        const response = await fetch("http://localhost:8100/api/models/")
        if (response.ok) {
            const data = await response.json();
            setModelList(data.models)
        }
        else {
            console.error(response);
        }
    }
    useEffect (() => {
        loadModel()
    }, []);

    return (
        <div>
        <h1>Models</h1>
        <table className="table table-striped">
        <thead>
            <tr>
            <th>Name</th>
            <th>Manufacturer</th>
            <th>Picture</th>
            </tr>
        </thead>
        <tbody>
            { modelList.map(model => {
                return (
                <tr key={model.id}>
                    <td>{ model.name }</td>
                    <td>{ model.manufacturer.name }</td>
                    <td>
                        <img src={ model.picture_url } className='w-25'/>
                    </td>
                </tr>
                );
            })}
        </tbody>
    </table>
    </div>
    )
}

export default ModelList
