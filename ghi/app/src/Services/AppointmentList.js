import React, { useEffect, useState } from 'react';


export default function AppointmentList() {
    const [appointments, setAppointments] = useState([]);
    const [autos, setAutos] = useState([]);
    const fetchData = async () => {
        const url = 'http://localhost:8080/api/appointments/';
        const response = await fetch(url);
        if (response.ok) {
            const data = await response.json();
            setAppointments(data.appointments);
        }
    };
    
    const fetchAutos = async () => {
        const url = "http://localhost:8100/api/automobiles";
        const response = await fetch(url);
        if (response.ok) {
            const data = await response.json();
            setAutos(data.autos);
        }
    };

    function getAppointments() {
        fetchData();
        fetchAutos();
    }

    function cancelAppointment(href) {
        fetch(`http://localhost:8080${href}/cancelled/`,{
            method: 'PUT',
            body: JSON.stringify(),
            headers: {
                "Content-Type": "application/json"
            }
        }).then((result) => {
            result.json().then((resp) => {
                console.warn(resp)
                getAppointments()
            })
        })
    }

    function completeAppointment(href) {
        fetch(`http://localhost:8080${href}/completed/`,{
            method:'PUT',
            body: JSON.stringify(),
            headers:{
                "Content-Type": "application/json"
            }
        }).then((result) => {
            result.json().then((resp) => {
                getAppointments()
            })
        })
    }

    function isVIP(appointment) {
        let isVIPCustomer = false;
        autos.forEach(auto => {
            if(appointment.vin === auto.vin) {
                isVIPCustomer = true;
            }
        });
        return isVIPCustomer
    }

    useEffect(() => {
        getAppointments();
    } , []);

    return (
        <div className="row">
            <div className="offset-3 col-6">
                <h1>Appointments</h1>
                <table className="table table-striped">
                    <thead>
                        <tr>
                            <th>VIN</th>
                            <th>VIP</th>
                            <th>Customer</th>
                            <th>Date</th>
                            <th>Time</th>
                            <th>Technician</th>
                            <th>Reason</th>
                            <th>Complete/Cancel</th>
                        </tr>
                    </thead>
                    <tbody>
                        {appointments.filter((appointment)=>appointment.cancelled===false&&appointment.completed===false).map((appointment, id) => (
                            <tr key={id} value={appointment.id}>
                                <td>{appointment.vin}</td>
                                <td>{isVIP(appointment) ? "yes": "no"}</td>
                                <td>{appointment.customer}</td>
                                <td>{appointment.date}</td>
                                <td>{appointment.time}</td>
                                <td>{appointment.technician}</td>
                                <td>{appointment.reason}</td>
                                <td>
                                <button onClick={() => completeAppointment(appointment.href)} type="button" className="btn btn-primary" style={{ marginRight: '10px' }}>Completed</button>
                                <button onClick={() => cancelAppointment(appointment.href)} type="button" className="btn btn-danger">Cancelled</button>
                                </td>
                            </tr>
                        ))}
                    </tbody>
                </table>
            </div>
        </div>
    );
}
