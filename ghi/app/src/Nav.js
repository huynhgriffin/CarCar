import { NavLink, Link } from 'react-router-dom';

function Nav() {
  return (
    <nav className="navbar navbar-expand-lg navbar-dark bg-success">
      <div className="container-fluid">
        <NavLink className="navbar-brand" to="/">CarCar</NavLink>
        <button className="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
          <span className="navbar-toggler-icon"></span>
        </button>
        <div className="collapse navbar-collapse" id="navbarSupportedContent">
          <ul className="navbar-nav me-auto mb-2 mb-lg-0">
            <li className="nav-item">
              <NavLink className="nav-link active" aria-current="page" to="/">
                Home
              </NavLink>
            </li>
            <li className="nav-item dropdown">
              <a
                className="nav-link dropdown-toggle"
                id="navbarDarkDropdownMenuLink"
                role="button"
                data-bs-toggle="dropdown"
                aria-expanded="false"
              >
                Inventory
              </a>
              <ul
                className="dropdown-menu"
                aria-labelledby="navbarDarkDropdownMenuLink"
              >
                <li>
                  <Link className="dropdown-item" to="models/new">
                    New Vehicle Model
                  </Link>
                </li>
                <li>
                  <Link className="dropdown-item" to="models">
                    Vehicle Models List
                  </Link>
                </li>
                <li>
                  <Link
                    className="dropdown-item"
                    to="automobiles/new"
                  >
                    New Automobile
                  </Link>
                </li>
                <li>
                  <Link className="dropdown-item" to="automobiles">
                    Automobile List
                  </Link>
                </li>
              </ul>
              </li>
            <li className="nav-item dropdown">
              <a
                className="nav-link dropdown-toggle"
                id="navbarDarkDropdownMenuLink"
                role="button"
                data-bs-toggle="dropdown"
                aria-expanded="false"
                >
                  Appointments
                </a>
              <ul
                className="dropdown-menu"
                aria-labelledby="navabrDarkDropDownMenuLink"
              >
                <li>
                  <Link className="dropdown-item" to="appointments">
                    Appointment List
                  </Link>
                </li>
                <li>
                  <Link className="dropdown-item" to="appointments/new">
                    New Appointment
                  </Link>
                </li>
                <li>
                  <Link className="dropdown-item" to="appointments/history">
                    Appointment History
                  </Link>
                </li>
              </ul>
            </li>
            <li className="nav-item dropdown">
              <a
                className="nav-link dropdown-toggle"
                id="navbarDarkDropdownMenuLink"
                role="button"
                data-bs-toggle="dropdown"
                aria-expanded="false"
                >
                  Technicians
                </a>
              <ul
                className="dropdown-menu"
                aria-labelledby="navabrDarkDropDownMenuLink"
              >
                <li>
                  <Link className="dropdown-item" to="technicians">
                    Technician List
                  </Link>
                </li>
                <li>
                  <Link className="dropdown-item" to="technicians/new">
                    New Technician
                  </Link>
                </li>
              </ul>
            </li>
          <li className="nav-item">
              <NavLink className="nav-link" to="/customer">Customers</NavLink>
            </li>
            <li className="nav-item">
              <NavLink className="nav-link" to="/customer/new"> Add a Customer</NavLink>
            </li>
            <li className="nav-item">
              <NavLink className="nav-link" to="/salespeople">Salespeople</NavLink>
            </li>
            <li className="nav-item">
              <NavLink className="nav-link" to="/salespeople/new">Add a Salesperson</NavLink>
            </li>
            <li className="nav-item">
              <NavLink className="nav-link" to="/manufacturers">Manufacturers</NavLink>
            </li>
            <li className="nav-item">
              <NavLink className="nav-link" to="/manufacturers/new">Create a Manufacturer</NavLink>
            </li>
            <li className="nav-item">
              <NavLink className="nav-link" to="/sales">Sales</NavLink>
            </li>
            <li className="nav-item">
              <NavLink className="nav-link" to="/sales/new">Create a Sale</NavLink>
            </li>
            <li className="nav-item">
              <NavLink className="nav-link" to="/sales/history">Salesperson History</NavLink>
            </li>
          </ul>
        </div>
      </div>
    </nav>
  )
}

export default Nav;
