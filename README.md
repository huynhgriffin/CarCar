# CarCar

Team:

* Ryan: service microservice
* Griffin: sales microservice

## Design
- Application to manage an automobile dealership
- Tracks inventory, sales, and services in separate microservices
- Uses RESTful API Django in the back-end
- Uses React in the front-end to display data and interface user interaction

## Diagram
![diagram](Project-beta-diagram.png)

## Installation

Navigate to the desired directory from your terminal and use git to clone this repository into your computer (Replace the "repo link" with the actual link to the repo).

```
$ git clone <repo link>
```

Install Docker desktop from https://www.docker.com and start the Docker application. Navigate into the **project directory** and run the following commands in your terminal.

```
docker volume create beta-data
docker-compose build
docker-compose up
```

**Note:** When running `docker-compose up` on macOS, you will see a warning about an environment variable name `OS` being missing. **You can safely ignore this.**

Once all the containers are up and running in your Docker desktop terminal, make and run migrations. Do **not** stop the services.

```
python manage.py makemigrations
python manage.py migrate
```

Restart the poller service using Docker desktop. You can navigate to http://localhost:3000/ in your browser to access your application.

## Inventory microservice

### Overview

The Inventory microservice allows users to manage the vehicle inventory within a dealership. Users can create and/or update `Manufacturer`, `VehicleModel`, and `Automobile` objects, delete existing records, and list and view the details for the objects in the inventory.

### Models

`Manufacturer`

- name

`VehicleModel`

- name
- picture_url
- manufacturer - Foreign key to `Manufacturer`

`Automobile`

- color
- year
- vin
- sold
- model - Foreign key to `VehicleModel`

### RESTful API (Port 8100):

### Manufacturers

You can access the manufacturer endpoints at the following URLs.
| Action | Method | URL | View Function |
| :--- | :--- | :--- | :--- |
| List manufacturers | GET | `http://localhost:8100/api/manufacturers/` | `api_manufacturers` |
| Create a manufacturer | POST | `http://localhost:8100/api/manufacturers/` | `api_manufacturers` |
| Get a specific manufacturer | GET | `http://localhost:8100/api/manufacturers/:id/` | `api_manufacturer` |
| Update a specific manufacturer | PUT | `http://localhost:8100/api/manufacturers/:id/` | `api_manufacturer` |
| Delete a specific manufacturer | DELETE | `http://localhost:8100/api/manufacturers/:id/` | `api_manufacturer`|

<details>
<summary><strong>Creating and updating a manufacturer</strong></summary>

```
{
  "name": "BMW"
}
```

</details>
<details>
<summary><strong>Creating, getting, and updating a single manufacturer</strong></summary>

```
{
  "href": "/api/manufacturers/1/",
  "id": 1,
  "name": "BMW"
}
```

</details>
<details>
<summary><strong>List of manufacturers</strong></summary>

```
{
  "manufacturers": [
    {
      "href": "/api/manufacturers/1/",
      "id": 1,
      "name": "BMW"
    }
  ]
}
```

</details>

### Vehicle Models

You can access the vehicle model endpoints at the following URLs.
| Action | Method | URL | View Function |
| :--- | :--- | :--- | :--- |
| List vehicle models | GET | `http://localhost:8100/api/models/` | `api_vehicle_models` |
| Create a vehicle model | POST | `http://localhost:8100/api/models/` | `api_vehicle_models` |
| Get a specific vehicle model | GET | `http://localhost:8100/api/models/:id/` | `api_vehicle_model` |
| Update a specific vehicle model | PUT | `http://localhost:8100/api/models/:id/` | `api_vehicle_model` |
| Delete a specific vehicle model | DELETE | `http://localhost:8100/api/models/:id/` | `api_vehicle_model` |

<details>
<summary><strong>Creating a vehicle model</strong></summary>

```
{
  "name": "M4",
  "picture_url": "https://upload.wikimedia.org/wikipedia/commons/thumb/a/af/BMW_M4_%28G82%29_IMG_4183.jpg/1920px-BMW_M4_%28G82%29_IMG_4183.jpg",
  "manufacturer_id": 1
}
```

</details>
<details>
<summary><strong>Updating a vehicle model</strong></summary>

```
{
  "name": "M5",
  "picture_url": "https://hips.hearstapps.com/hmg-prod/images/2024-bmw-m5-sedan-rendering-1676575610.jpg"
}
```

</details>
<details>
<summary><strong>Getting, creating, or updating a vehicle model</strong></summary>

```
{
  "href": "/api/models/1/",
  "id": 1,
  "name": "M4",
  "picture_url": "https://upload.wikimedia.org/wikipedia/commons/thumb/a/af/BMW_M4_%28G82%29_IMG_4183.jpg/1920px-BMW_M4_%28G82%29_IMG_4183.jpg",
  "manufacturer": {
    "href": "/api/manufacturers/1/",
    "id": 1,
    "name": "BMW"
  }
}
```

</details>
<details>
<summary><strong>List of vehicle models</strong></summary>

```
{
  "models": [
    {
      "href": "/api/models/1/",
      "id": 1,
      "name": "M4",
      "picture_url": "https://upload.wikimedia.org/wikipedia/commons/thumb/a/af/BMW_M4_%28G82%29_IMG_4183.jpg/1920px-BMW_M4_%28G82%29_IMG_4183.jpg",
      "manufacturer": {
        "href": "/api/manufacturers/1/",
        "id": 1,
        "name": "BMW"
      }
    }
  ]
}
```

</details>

### Automobile Information

You can access the automobile endpoints at the following URLs.
| Action | Method | URL | View Function |
| :--- | :--- | :--- | :--- |
| List automobiles | GET | `http://localhost:8100/api/automobiles/` | `api_automobiles` |
| Create an automobile | POST | `http://localhost:8100/api/automobiles/` | `api_automobiles` |
| Get a specific automobile | GET | `http://localhost:8100/api/automobiles/:vin/` | `api_automobile` |
| Update a specific automobile | PUT | `http://localhost:8100/api/automobiles/:vin/` | `api_automobile` |
| Delete a specific automobile | DELETE | `http://localhost:8100/api/automobiles/:vin/` | `api_automobile` |

<details>
<summary><strong>Creating an automobile</strong></summary>

```
{
  "color": "white",
  "year": 2007,
  "vin": "WBAVC11037VE19152",
  "model_id": 1
}
```

</details>
<details>
<summary><strong>Details for a specific automobile</strong></summary>

```
{
  "href": "/api/automobiles/WBAVC11037VE19152/",
  "id": 1,
  "color": "white",
  "year": 2007,
  "vin": "WBAVC11037VE19152",
  "model": {
    "href": "/api/models/1/",
    "id": 1,
    "name": "M4",
    "picture_url": "https://upload.wikimedia.org/wikipedia/commons/thumb/a/af/BMW_M4_%28G82%29_IMG_4183.jpg/1920px-BMW_M4_%28G82%29_IMG_4183.jpg",
    "manufacturer": {
      "href": "/api/manufacturers/1/",
      "id": 1,
      "name": "BMW"
    }
  }
}
```

</details>
<details>
<summary><strong>Updating the automobile</strong></summary>

```
{
  "color": "red",
  "year": 2023
}
```

</details>
<details>
<summary><strong>List of Automobiles</strong></summary>

```
{
  "autos": [
    {
      "href": "/api/automobiles/WBAVC11037VE19152/",
      "id": 1,
      "color": "white",
      "year": 2007,
      "vin": "WBAVC11037VE19152",
      "model": {
        "href": "/api/models/1/",
        "id": 1,
        "name": "M4",
        "picture_url": "https://upload.wikimedia.org/wikipedia/commons/thumb/a/af/BMW_M4_%28G82%29_IMG_4183.jpg/1920px-BMW_M4_%28G82%29_IMG_4183.jpg",
        "manufacturer": {
          "href": "/api/manufacturers/1/",
          "id": 1,
          "name": "BMW"
        }
      }
    }
  ]
}
```

</details>

## Service microservice

### Overview

This service allows users to create a new technician, view a list of technicians, create a new service appointment, view an appointment list, and view the history of service appointments. After creating a technician, it is stored to the database and can be accessed via a drop-down menu when creating an appointment. When creating a new service appointment, it will be saved to the database and accessible in both the Appointment List and the Appointment History. When viewing the list of appointments, each appointment has a "cancelled" and "completed" button along with the relevant appointment information. If the appointment is completed or cancelled, the user can click the appropriate button and the appointment will disappear from the list. The appointment will still remain in the database and can be accessed in the Appointment History. Each appointment can display whether the customer purchased the vehicle from the dealership, marking them as a VIP. The Appointment History allows a user to input a VIN and view all appointments filtered by the VIN. This microservice utilizes a poller to retrieve automobile data from the Inventory.

### Models

`Technician`

- name
- employee_id

`Appointment`

- customer
- reason
- date
- time
- completed
- cancelled
- vin - Foreign key to `AutomobileVO`
- technician - Foreign key to `Technician`

### RESTful API (Port 8080):

You can access the service endpoints at the following URLs.

### Technician

| Action                                | Method | URL                                          | View Function     |
| :------------------------------------ | :----- | :------------------------------------------- | :---------------- |
| List technicians                      | GET    | `http://localhost:8080/api/technicians/`     | `api_technicians` |
| Create a technician                   | POST   | `http://localhost:8080/api/technicians/`     | `api_technicians` |
| Get a specific technician             | GET    | `http://localhost:8080/api/technicians/:id/` | `api_technician`  |
| Update a specific technician | PUT    | `http://localhost:8080/api/technicians/:id/` | `api_technician`  |
| Delete a specific technician          | DELETE | `http://localhost:8080/api/technicians/:id/` | `api_technician`  |

<details>
<summary><strong>Creating a technician</strong></summary>

```
{
  "name": "Walter",
  "employee_id": "00001"
}
```

</details>
<details>
<summary><strong>Getting, creating, or updating a technician</strong></summary>

```
{
	"href": "/api/technicians/4/",
	"name": "Jacob",
	"employee_id": "00002"
}
```

</details>
<details>
<summary><strong>List of vehicle models</strong></summary>

```
{
	"technicians": [
		{
			"href": "/api/technicians/4/",
			"name": "Jacob",
			"employee_id": "00002"
		}
	]
}
```

</details>

### Appointment

| Action                                 | Method | URL                                           | View Function      |
| :------------------------------------- | :----- | :-------------------------------------------- | :----------------- |
| List appointments                      | GET    | `http://localhost:8080/api/appointments/`     | `api_appointments` |
| Create an appointment                  | POST   | `http://localhost:8080/api/appointments/`     | `api_appointments` |
| Get a specific appointment             | GET    | `http://localhost:8080/api/appointments/:id/` | `api_appointment`  |
| Update a specific appointment          | PUT    | `http://localhost:8080/api/appointments/:id/` | `api_appointment`  |
| Delete a specific appointment          | DELETE | `http://localhost:8080/api/appointments/:id/` | `api_appointment`  |

<details>
<summary><strong>Creating an appointment</strong></summary>

```
{
  "vin": "WBAVC11037VE19152",
  "customer": "Haywood",
  "date": "2023-04-28",
  "time": "16:20",
	"technician": "Walter",
	"reason": "Oil change"
}
```

</details>
<details>
<summary><strong>Getting, creating, or updating an appointment</strong></summary>

```
{
	"href": "/api/appointments/1/",
	"vin": "WBAVC11037VE19152",
  "customer": "Haywood",
  "date": "2023-04-28",
  "time": "16:20",
	"technician": "Alfalfa",
	"reason": "Oil change"
	"completed": false,
	"cancelled": false
}
```

</details>
<details>
<summary><strong>List of vehicles</strong></summary>

```
{
	"appointments": [
		{
      "href": "/api/appointments/1/",
      "vin": "WBAVC11037VE19152",
      "customer": "Haywood",
      "date": "2023-04-28",
      "time": "16:20",
      "technician": "Walter",
      "reason": "Oil change"
      "completed": false,
      "cancelled": false
    },
	]
}
```

</details>



# Sales microservice

## How to Run this Project

1. Fork and git clone the repository link with HTTPS into your terminal and folder you wish for it to be in
2. You want to build your volumes. images, and containers using the three commands
    - docker volume create beta-data
    - docker-compose build
    - docker-compose up
3. After setting up your containers and images you want to go to localhost:3000 and localhost:8090 to see that they are running properly



## API Documentation

So we have made four models for our sales microservice. The main four that we have are:
- Customer model data wants first name, last name, address, and phone number.
    the body of the data should look like this
    ```
    {
	"first_name": "Jonny",
	"last_name": "Sins",
	"address": "76 Carruth St, Dorchester",
	"phone_number": "447-258-3133"
    }
    ```
- Salesperson model data wants first name, last name, and employee id.
    the body of the data should look like this
    ```
    {
	"first_name": "Gary",
	"last_name": "Vee",
	"employee_id": "gvee"
    }
    ```
- AutomobileVO model data only wants the vin from inventory microservice.

- Sale wants price also having foreign key of salesperson, automobile, and customer
    the body of the data should look like this
    ```
    {
	"price": 30000,
	"automobile": "3C3CC5FB2AN120174",
	"salesperson": 11,
	"customer": 4
    }
    ```

And what we are doing with these models is that we are making sure that each model have fields that has some sort of factor to tell it what kind of data it should be.

We use Insomnia to create the data, see the data, and delete the data that we create or can create. There are three methods that we use in insomnia that are able to do this:
- GET (example link: http://localhost:8090/api/sales/)
- POST (example link: same link as get)
- DELETE (example link: http://localhost:8090/api/salespeople/:id)

GET is able to show us the list of the data that we have currently in our database

POST is able to create the piece of data that we want to put into our database

DELETE is able to remove a piece of data from our database
